<?php 
	class Admin_main{
		private $_model;
        private $edit_data;
        
        public function __construct(){
			$this->_model = new Model_Main();
        }
        
        public function adminLoad(){            
            $this->edit_data['list_ctlog'] = $this->_model->S_ctalog(1);                     
            
            include('views/ctlog_chnge.tmp');
			foreach($this->edit_data['list_ctlog'] as $key => $value){
                print_r('<option value="'.$value['name'].'">'.$value['name'].'</option>');
			}		
            print_r('</select></div> <div id="nt3" class="p_box"> <div id="nt10"> ');           
    		$this->Redraw(1);            
            include('views/clicknew.tmp');
        }
        
        public function SignEmployee($data){
            $encrypted_mypassword=md5($data['pass']);
            
            return $this->edit_data['view_role'] = $this->_model->viewRole($data['login'],$encrypted_mypassword);
        }
        
        public function viewOrders(){
            $this->content_data['view_order'] = $this->_model->ShowOrders();
            foreach( $this->content_data['view_order'] as $key => $value) {
                $this->content_data['ordr2'] = $this->_model->qryOrd1($value['prdct_id'],$value['persinfid']);
                foreach( $this->content_data['ordr2'] as $key => $value2)
                include '../manager/vieworder.tmp';
                // $this->Render("vieworder",$value,$value2,NULL,"../../");
             }
        }
        
        public function EditPrdct($i){
          
            switch($i){
                case 1:
                    $this->edit_data['edit_view'] = $this->_model->S_prduct(1,$_GET['edit']);
                    $this->Render("views/hdr2",$this->edit_data['edit_view'][0]);
                    break;
                case 2:
                    $this->Render("views/hdr2_1");
                    break;                    
                case 3:
                    $num = $this->_model->S_ctalog(1,$_GET['change_ctlog']);
                    $this->Redraw($num[0]['id']);
                    break;  
                case 4:
                    $num = $this->_model->S_prduct(1,$_GET['remove']);                
                    $this->_model->RmfromCtlg();
                    $this->Redraw($num[0]['catalogid']);
                    break;      
            }
        }
    
         public function addP($b,$idpr=NULL){
              switch($b){
                  case 1:
                    $this->edit_data['list_mnfturer'] = $this->_model->S_mnufcturer();

                    if(isset($idpr))
                        foreach($this->edit_data['list_mnfturer'] as $key => $value)
                            if($value['id']==$idpr)
                                print_r('<option value="'.$value['name'].'" selected>'.$value['name'].'</option>');
                            else
                                print_r('<option value="'.$value['name'].'" >'.$value['name'].'</option>');
                    else
                        foreach($this->edit_data['list_mnfturer'] as $key => $value)
                            print_r('<option value="'.$value['name'].'" >'.$value['name'].'</option>');                  
                    break;
                  case 2:
                      $this->edit_data['list_ctlog'] = $this->_model->S_ctalog(1);
                      foreach($this->edit_data['list_ctlog'] as $key => $value)
                            print_r('<option value="'.$value['name'].'" >'.$value['name'].'</option>');
                  
                    break;                    
              }
        }
    
       public function Update_Data($i){
            $count = $this->_model->S_mnufcturer(NULL,$_POST['choice'])[0]["id"];
            switch($i){
               case 1:                    
                    $this->_model->NewOld($count);
                    $this->Redraw($_POST['catalogid']);
                    break;
               case 2:
                    $ctlog_id = $this->_model->S_ctalog(1,$_POST['choice_ctlog'])[0]["id"];
                    $this->new_data['newid'] = $this->_model->S_prduct(4);                    
                    $idrnd = $this->gen($this->new_data['newid']);
                    
                    $this->_model->AddnewCntnt($count,$idrnd,$ctlog_id);
                    $this->Redraw($ctlog_id);                    
                    break;
               }
        }
    
    
         public function gen($row){
            $num = mt_rand(10000000,99999999);
            foreach($row as $key => $value){
                if($num == $value['id'])
                    $this->gen();
            }            
            return $num;
        }    
        
        
        public function Redraw($ctid){
            $varB = 0;
            $this->edit_data['list_prduct'] = $this->_model->S_prduct(1,NULL,$ctid);
			foreach($this->edit_data['list_prduct'] as $key => $value){
                $this->edit_data['list_prducer'] = $this->_model->S_mnufcturer($value['prducerid']);
                $this->Render("views/mainplace",$value,$this->edit_data['list_prducer'][0],$varB);
                $varB++;
            }
        }
        
        
	    private function Render($tmp_name,$row=NULL,$tmp=NULL,$tmp2=NULL){
			include $tmp_name.'.tmp';
		}
        
       
        
    
	}
 
    
 ?>