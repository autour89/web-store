<?php
    $id = "SESSID";
    session_name($id);
    session_start();
    
   function __autoload($class_name){
        if(file_exists("../../models/".$class_name.".php"))
            require "../../models/".$class_name.".php";
        elseif(file_exists("../controllers/".$class_name.".php"))
            require "../controllers/".$class_name.".php";
        else
			die( 'file with class < '.$class_name.' > not found ' ); 
	}
    
   if(!isset($_SESSION['admin']) && $_SESSION['admin']['isloggedin'] == FALSE)
        header('location: ../controllers/login/tmo.php');
   elseif(isset($_GET['action']) && $_GET['action'] == "clear_login"){
        $_SESSION['admin']['isloggedin']= FALSE;
		session_unset();
		session_destroy();
        header('location: ../controllers/login/tmo.php');
     }
     
    $bagst = new Admin_main();
        
?>

<html>
    <title>Adminka</title>
       <head>
       <?php  include 'views/footer.html';?>        
      <title>Store</title>      
   </head>
   
    <body>	
           
    <?php
        $bagst->adminLoad();
    ?>
  
    </body>	
</html>
