<?php

    function __autoload($class_name){
        if(file_exists("../controllers/".$class_name.".php"))
            require "../controllers/".$class_name.".php";
        elseif(file_exists("../../models/".$class_name.".php"))
            require "../../models/".$class_name.".php";
        else
			die( 'file with class < '.$class_name.' > not found ' ); 
	}
    
   $id = "SESSID";
   session_name($id);
   session_start();    
   if(!isset($_SESSION['admin']) && $_SESSION['admin']['isloggedin'] == FALSE)
        header('location: ../controllers/login/tmo.php');
   elseif(isset($_GET['action']) && $_GET['action'] == "clear_login"){
        $_SESSION['admin']['isloggedin']= FALSE;
		session_unset();
		session_destroy();
        header('location: ../controllers/login/tmo.php');
     }

   $bagst = new Admin_main();
   
   
?>

<html>
   
   <head>
      <title>Adminka</title>
   </head>
   
   <body>

<style type="text/css">
	.order_wrap{
		width: 200px;
		padding: 50px;
		border: 1px solid black;
		display: inline-block;
	}
    
    .ord{
        width: 75%;
        left: 100;
        top: 50;
        padding: 50px;
        border: 1px solid black;
        position: relative;
             
    }
    
    .fr{
        padding: 5px;
        border: 2px solid #5499C7;
        position: absolute;
        background-color: #F7DC6F;
      }
      a {text-decoration: none;}
	  a:visited {text-decoration: none;}
   }
	
</style>
<div class="fr">
<a href="index.php?action=clear_login">logout</a>    
</div>
<div class="ord">
<?php $bagst->viewOrders();?>
</div>
   </body>
   
</html>