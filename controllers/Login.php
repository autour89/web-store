<?php
    class Login {
        private $bagst;
        public function __construct () {            
            $this->bagst = new Content_main();
          }
        
        public function SetSn(){
            session_name("SESSID");
            session_start();
            ini_set('session.gc_maxlifetime', 8*60*60);
            ini_set('session.gc_probability', 1);
            ini_set('session.gc_divisor', 100);
        }
        
        public function UnSetsn(){
            unset($_SESSION['userinfo']);
            session_destroy();
        }
        
        public function Login($i){
            
            switch($i){
                case 1:
                    $this->bagst->LgntoAccnt($_SESSION['userinfo']['username'],$_SESSION['userinfo']['userpswd']);
                    break;
                case 2:
                    $this->dataView($this->bagst->LgntoAccnt($_POST['lgn_n'],md5($_POST['pswd_f'])),$_POST['lgn_n'],md5($_POST['pswd_f']));
                    break;
            }
        }

        public function Logout(){
            $_SESSION['userinfo']['isloggedin'] = FALSE;
        } 
        
        public function Setsign(){
            $this->bagst->sign_newAccnt();
            $this->dataView($this->bagst->LgntoAccnt($_POST['sgnlgn_n'],md5($_POST['sgnpswd_f'])),$_POST['sgnlgn_n'],md5($_POST['sgnpswd_f']));
        }
        
        private function dataView($uid,$lgn,$pswd){
            $userinfo = array();
            
            $userinfo['username'] = $lgn;
            $userinfo['userpswd'] = $pswd;
            $userinfo['UID'] = $uid;
            $userinfo['isloggedin'] = TRUE;
            $_SESSION['userinfo'] = $userinfo;
        }
        
 
    }
?>