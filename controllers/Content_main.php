<?php 
	class Content_main{
		private $_model;
		private $content_data;
        private $info;
        private $wishL;
        
	  public function __construct(){
			$this->_model = new Model_Main();
            
            $this->info = array();
            $this->wishL = array();
	  }
        
      public function actionCatalog(){
           $this->content_data['main_menu'] = $this->_model->S_ctalog(1);
           
           foreach( $this->content_data['main_menu'] as $key => $value)
              print_r('<li><a href="?category='.$value["id"].' " >'.$value["name"].' </a> </li>');
		}
        
	 public function actionGoods($i){
            switch($i){
               case 2:
                    $varB = 0;
                    $this->content_data['ctlg_goods'] = $this->_model->S_prduct($i);
                    print_r("<div class='show_goodsB'>");
                    foreach( $this->content_data['ctlg_goods'] as $key => $value){
                        $this->content_data['pics2'] = $this->_model->ViewPic($value['id']);
                        // $this->Render("goods_box",$value,$varB,$this->content_data['pics2']);
                        include 'views/box/goods_box.tmp';
                        $varB++;
                    }
                    print_r("</div>");
                    break;
               case 3:
                    $this->content_data['goods'] = $this->_model->S_prduct($i);
                    $this->content_data['pics'] = $this->_model->ViewPic($_GET['id']);               
                    print_r("<div class='show_goodsB'>");
                    foreach( $this->content_data['goods'] as $key => $value)
                        print_r($value['name'].' : '.$value['prdct_num'].' : '.$value['dscrptn']);
                    
                    foreach( $this->content_data['pics'] as $key => $value)
                        include 'views/box/pic.tmp';
                    
                    print_r("</div>");
                    break;
              }
		}
        //------------------------------------------------------------------------------

       public function respondBascket($b){
           $varB = 0;  
           if(isset($_SESSION['wishL']))
             for($i=0;$i<count($_SESSION['wishL']);$i++){
                $this->content_data['bascket'] = $this->_model->S_prduct(1,$_SESSION['wishL'][$i]['pr_id']);
                 foreach( $this->content_data['bascket'] as $key => $value)
                     switch($b){
                        case 1:
                            include 'views/bascket/bs.tmp';   
                            break;
                        case 2:
                            include '../views/bascket/bs.tmp';
                            break;
                    }
                   $varB++;
             }
		}
       
       public function actionOrders($i){
           switch($i){
               case 1:
                    $this->content_data['newuser'] = $this->_model->ViewAccnt(2);
                    $idr = $this->gen($this->content_data['newuser']);
                    $this->_model->AddToOrders($idr);
                    $_SESSION['wishL'] = NULL;
                    break;
               case 2: 
                    $tmpcount=1;                    
                    
                    if(isset($_SESSION['wishL'])){
                     for($i=0;$i<count($_SESSION['wishL']);$i++)
                        if($_SESSION['wishL'][$i]['pr_id']==$_POST['good_id']){
                            $tmpcount++;
                            $_SESSION['wishL'][$i]['qntty']++;
                        }                            
                        if($tmpcount==1){
                            for($i=0;$i<count($_SESSION['wishL']);$i++){
                                $this->info['pr_id'] = $_SESSION['wishL'][$i]['pr_id'];
                                $this->info['qntty'] = $_SESSION['wishL'][$i]['qntty'];                    
                                array_push($this->wishL, $this->info);
                            }
                            $this->info['pr_id'] = $_POST['good_id'];
                            $this->info['qntty'] = $tmpcount;                    
                            array_push($this->wishL, $this->info);
                            $_SESSION['wishL'] = $this->wishL;     
                        }                        
                    }
                    else{
                        $this->info['pr_id'] = $_POST['good_id'];
                        $this->info['qntty'] = $tmpcount;                    
                        array_push($this->wishL, $this->info);
                        $_SESSION['wishL'] = $this->wishL;                             
                    }
                    break;
                case 4:
                    if(isset($_SESSION['wishL']))
                     for($i=0;$i<count($_SESSION['wishL']);$i++)
                        if($_SESSION['wishL'][$i]['pr_id']==$_GET['remove'])
                            array_splice($_SESSION['wishL'],$i,1);
                        
                    break;
           }
        }        
        //------------------------------------------------------------------------------
        
        public function LgntoAccnt($lgn, $pswd){
            $id;
            $this->login_data['ldta'] = $this->_model->ViewAccnt(1,$lgn,$pswd);
            if(count($this->login_data['ldta'])==1){
                foreach($this->login_data['ldta'] as $key => $value){
                    print_r($value['firstname']." ".$value['lastname']." ".$value['email']."<br><br>");
                    $id = $value['id'];
                }            
                $this->login_data['hstr'] = $this->_model->ViewHstry($id);
                
                foreach($this->login_data['hstr'] as $key => $value)
                    print_r($value['quantity']." ".$value['prdct_num'].'<br><br>');
                
                include '../views/lgn/logoutbtn.tmp';
                
             return $id;   
            }
            else
                include '../views/lgn/hdr.tmp';            
        }
        
        public function sign_newAccnt(){
            $this->sign_data['newstr'] = $this->_model->ViewAccnt(2);
            $idrnd = $this->gen($this->sign_data['newstr']);
            
            $this->_model->Addsign($idrnd);            
        }
                           
        public function gen($row){
            $num = mt_rand(100000,999999);
            foreach($row as $key => $value){
                if($num == $value['id'])
                    $this->gen();
            }            
            return $num;
        }         

       private function Render($tmp_name,$row=NULL,$tmp=NULL,$tmp2=NULL,$dir=FALSE){
			include $dir.'views/'.$tmp_name.'.tmp';
		}
      
	}
 
    
 ?>