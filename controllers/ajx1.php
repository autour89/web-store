<?php
   
    function __autoload($class_name){
        if(file_exists("../models/".$class_name.".php"))
            require "../models/".$class_name.".php";
        elseif(file_exists($class_name.".php"))
            require $class_name.".php";
        else
			die( 'file with class < '.$class_name.' > not found ' ); 
	}

    $lgn = new Login();
    $lgn->SetSn();
    $bagst = new Content_main();
 
    //----------------------------------------------------------
    if(isset($_POST['good_id'])){
        $bagst->actionOrders(2);    
        $bagst->respondBascket(2);
    }
  elseif(isset($_GET['remove'])){
        $bagst->actionOrders(4);
        $bagst->respondBascket(2);
        $_GET = NULL;
  }
        
    //----------------------------------------------------------
    if(isset($_GET['lquery']))
        switch($_GET['lquery']){
            case 'viewsgn_in':
                if(isset($_SESSION['userinfo']) and $_SESSION['userinfo']['isloggedin'] == TRUE )
                    $lgn->Login(1);
                 else
                    infoCh();
                break;            
            case 'viewsgn_up':
                Render("../views/lgn/hdr2");
                break;
            case 'log_out':
                $lgn->Logout();
                infoCh();                
                break;
        }    
    elseif(isset($_POST['lgn_n']) and isset($_POST['pswd_f']))
           $lgn->Login(2);
    elseif(isset($_POST['sgnlgn_fnm']) and isset($_POST['sgnlgn_n']) and isset($_POST['sgnlgn_eml']) and isset($_POST['sgnpswd_f']) )
        $lgn->Setsign();
    
    $_POST = NULL;
    function infoCh(){
        if(isset($_SESSION['userinfo']) and $_SESSION['userinfo']['isloggedin'] == FALSE )
            Render("../views/lgn/hdr3");        
        else
            Render("../views/lgn/hdr");
    }
    
    function Render($tmp_name){
			include $tmp_name.'.tmp';
            $_GET = NULL;
	}    
    
?>