<?php
    
    $id = "SESSID";
    session_name($id);
    session_start();
    ini_set('session.gc_maxlifetime', 8*60*60);
    ini_set('session.gc_probability', 1);
    ini_set('session.gc_divisor', 100);
    date_default_timezone_set('Europe/Kiev');
   
   function __autoload($class_name){
        if(file_exists("models/".$class_name.".php"))
            require "models/".$class_name.".php";
        elseif(file_exists("controllers/".$class_name.".php"))
            require "controllers/".$class_name.".php";
        else
			die( 'file with class < '.$class_name.' > not found ' ); 
	}

    $bagst = new Content_main();   
   
?>


<html> 
   <head>
       <?php include 'views/footer.html'; ?>        
      <title>Store</title>     
   </head>
   
 <body> 

  <?php
  if(!isset($_GET['id']) and !isset($_GET['category']))
      $bagst->actionGoods(2);
  
  if(isset($_POST['prdcnm']) && count($_POST['prdcnm'])>0)
        $bagst->actionOrders(1);
    
   if(isset($_GET['id']))
       $bagst->actionGoods(3);          
   if(isset($_GET['category']))
       $bagst->actionGoods(2);
   
   $_POST=NULL;
  ?>
  
 </body>
 
</html>