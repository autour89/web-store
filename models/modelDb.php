<?php

 
 class  ModelDB{
     private static $_link;
       
     public function __construct () {
      }
        
        
     public function NextCon($i){
         switch ($i) {
            case 'start':
                $this->dbconn = mysqli_connect(Config::DB_HOST, Config::DB_USER, Config::DB_PASS, Config::DB_NAME)
			         or die('MySQL connect failed. '.mysqli_connect_error());
                break;
            case 'stop':
                mysqli_close($this->dbconn);
                break;
        }
      }
      public function NQuery($query){                 
         return mysqli_query($this->dbconn,$query);
      }
            
            
      public function FetchArray($fetch){          
          return mysqli_fetch_array($fetch);
      }
      
      public static function getLink(){
			if(is_null(self::$_link)){
				self::$_link = new ModelDB();
			}
			return self::$_link;
		}
        
        

  }
 
?>